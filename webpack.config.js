const webpack = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");

const nodeExternals = require("webpack-node-externals");
const path = require('path');
const srcDir = './src/';

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  entry: {
    tabs: path.join(__dirname, srcDir + 'index.ts'),
    options: path.join(__dirname, `${srcDir}options.ts`)
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  optimization: {
    splitChunks: {
      name: 'vendor',
      chunks: "initial"
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.tsx' ]
  },
  externals: [ nodeExternals() ],
  plugins: [
    new CopyPlugin({ 
      patterns: [{ from: "./manifest.json", to: "./manifest.json" },
                 { from: "tab-24px.svg", to: "tab-24px.svg"},
                 { from :"static", to: "" }]
    })
  ]
};
