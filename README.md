# Tab Tallier. Tally your Tabs!

Count how many times a tab gets opened or closed in Chrome

## Development Getting Started

**Dev Stack**

- Typescript
- Webpack
- Vanilla DOM functions babyyyyyyy

### 1 - Install Dependencies

```
npm install
```

### 2 - Assemble your bundle

```
npm run build
```

This will spit out two javascript bundles

- tabs.js - this is the special sauce. the main handler for all tab-related operations
- options.js - this is just a light wrapper on the options page to store configurations

### 3 - Load into Chrome

Look at [the Chrome Extension](https://developer.chrome.com/extensions/getstarted) docs on how to add this to your Chrome.

