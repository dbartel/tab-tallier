
export interface TallyTab {
    readonly tabsOpened: number;
    readonly tabsClosed: number;
    readonly date: string;
}

export function defaultTally(): TallyTab {
    return {
        tabsOpened: 0,
        tabsClosed: 0,
        date: new Date().toISOString()
    };
}

