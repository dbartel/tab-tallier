import { retrieveFromStorage, writeToStorage } from "./chrome-facade";

export const OPTION_KEY = "tally-config";


export interface Config {
    readonly name: string;
    readonly slackHook: string;
}


export const defaultConfig: Config = {
    name: "",
    slackHook: ""
};

export const loadConfig = () => {
    return retrieveFromStorage(OPTION_KEY, defaultConfig)
}

export const writeConfig = (config: Config) => {
    return writeToStorage(OPTION_KEY, config)
}
