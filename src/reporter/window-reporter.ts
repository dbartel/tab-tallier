import { ReportFunc } from './model';

export const windowReporter: ReportFunc = report => new Promise<boolean>((resolve, reject) => {
    alert(`Tabs Opened: ${report.tabsOpened}
Tabs Closed: ${report.tabsClosed}
Current Tab Count: ${report.currentTabCount}`);

    resolve(true);
});


