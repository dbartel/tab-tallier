import { TabReport, ReportFunc } from './model';

// export const slackReporter: (string) => ReportFunc

export function createSlackReporter(url: string): ReportFunc {
    return (report: TabReport) => {
        return fetch(url, {
            method: "POST",
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                text: `Tabs Opened: ${report.tabsOpened}
Tabs Closed: ${report.tabsClosed}
Current Tab Count: ${report.currentTabCount}`
            })
        })
            .then(res => true);

    };
}
