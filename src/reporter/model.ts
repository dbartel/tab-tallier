
export interface TabReport {
    readonly tabsOpened: number;
    readonly tabsClosed: number;
    readonly currentTabCount: number;
}

export interface ReportFunc {
    (report: TabReport): Promise<boolean>
}

export type ReportAggregator = ReportFunc[]
