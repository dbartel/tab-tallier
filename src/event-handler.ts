
import { getAllTabs, retrieveFromStorage, writeToStorage } from './chrome-facade';
import { TallyTab, defaultTally } from './persistance/model';
import { TabReport } from './reporter/model';
import { createServiceLocator, ServiceLocator } from './service-locator';

const CHROME_STORAGE_KEY = "tally-tab";
let serviceLocator: ServiceLocator;

export function appInstalled() {
    createServiceLocator()
        .then(locator => {
            serviceLocator = locator;
        });
}

export function tabAdded() {
    retrieveFromStorage(CHROME_STORAGE_KEY, defaultTally())
        .then(tally => {
            const newTally = {
                ...tally,
                tabsOpened: tally.tabsOpened + 1
            };

            return writeToStorage(CHROME_STORAGE_KEY, newTally);
        });
}


export function tabClosed() {
    retrieveFromStorage(CHROME_STORAGE_KEY, defaultTally())
        .then(tally => {
            const newTally = {
                ...tally,
                tabsClosed: tally.tabsClosed + 1
            };

            return writeToStorage(CHROME_STORAGE_KEY, newTally);
        });
}

export function iconClicked() {
    Promise.all([getAllTabs(), retrieveFromStorage(CHROME_STORAGE_KEY, defaultTally())])
        .then(results => {
            const tabCount = results[0].length;
            const tally = results[1];

            const report: TabReport = {
                tabsOpened: tally.tabsOpened,
                tabsClosed: tally.tabsClosed,
                currentTabCount: tabCount
            }

            return Promise.all(serviceLocator.reporters.map(r => {
                r(report);
            }));
        });
}


