import { ReportAggregator } from './reporter/model';
import { windowReporter } from './reporter/window-reporter';
import { createSlackReporter } from './reporter/slack-reporter';
import { loadConfig } from './config';


export interface ServiceLocator {
    readonly reporters: ReportAggregator
}

export function createServiceLocator(): Promise<ServiceLocator> {

    return loadConfig()
        .then(config => {
            return {
                reporters: [
                    windowReporter,
                    createSlackReporter(config.slackHook)
                ]
            }
        });
}
