/**
 * Contains facades for the chrome APIs
 * The chrome functions depend on callback functions, these functions wrap them in promises, making them easier to deal with elsewhere
 */

export async function retrieveFromStorage<T>(key: string, defaultValue: T): Promise<T> {
    const myQueryObject = {};
    myQueryObject[key] = defaultValue;

    var p = new Promise<T>((resolve, reject) => {
        chrome.storage.sync.get(myQueryObject, (result) => {
            resolve(result[key] as T);
        });
    });

    return p;
}

export async function writeToStorage<T>(key: string, value: T): Promise<T> {
    const myQueryObject = {};
    myQueryObject[key] = value;

    var p = new Promise<T>((resolve, reject) => {
        chrome.storage.sync.set(myQueryObject, () => {
            resolve(value);
        });
    });

    return p;
}

export async function getAllTabs(): Promise<any[]> {
    return new Promise<any>((resolve) => {
        chrome.tabs.query({}, tabs => {
            resolve(tabs);
        });
    });
}
