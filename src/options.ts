
import { loadConfig, writeConfig } from './config';

const optionName = <HTMLInputElement>document.querySelector("#option-name");
const optionSlack = <HTMLInputElement>document.querySelector("#option-slack");

loadConfig()
    .then(config => {
        optionName.value = config.name;
        optionSlack.value = config.slackHook;
    });

optionName.addEventListener("change", evt => {
    loadConfig()
        .then(config => {
            return {
                ...config,
                name: optionName.value
            };
        })
        .then(config => {
            return writeConfig(config);
        });
});

optionSlack.addEventListener("change", evt => {
    loadConfig()
        .then(config => {
            return {
                ...config,
                slackHook: optionSlack.value
            };
        })
        .then(config => {
            return writeConfig(config);
        });
});

