import { tabAdded, tabClosed, iconClicked, appInstalled } from './event-handler';

const isDebug = !("update_url" in chrome.runtime.getManifest());

function debugLog(str: string) {
    if (isDebug) {
        console.log(str);
    }
}

chrome.runtime.onInstalled.addListener(() => {
    debugLog("runtime.onInstalled");
    appInstalled();
});

chrome.tabs.onCreated.addListener(() => {
    debugLog("tabs.onCreated");
    tabAdded();
});

chrome.tabs.onRemoved.addListener(() => {
    debugLog("tabs.onRemoved");
    tabClosed();
});

chrome.browserAction.onClicked.addListener(tab => {
    debugLog("browserAction.onClicked");
    iconClicked();
});

